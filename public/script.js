var myModal = document.getElementById('myModal')
var myInput = document.getElementById('myInput')

myModal.addEventListener('shown.bs.modal', function () {
  myInput.focus()
})


function populateModal(index) {
    var innerModal = ['<div class="modal-dialog modal-dialog-scrollable">'+
    '<div class="modal-content">'+
        '<div class="modal-header">'+
            '<h5 class="modal-title" id="newModalLabel">'+
                'University of Central Missouri (Bachelor of Science, Dietetics)'+
            '</h5>'+
            '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">'+
            '</button>'+
        '</div>'+
        '<div class="modal-body">'+
            'Semester of Music (AE 1400)<br />'+
            'Principles of Macroeconomics (Econ 1010)<br />'+
            'Composition 1 (Engl 1020)<br />'+
            'Principles of Literacy Composition (Engl 1080)<br />'+
            'General Electronics (ET 1020)<br />'+
            'College Algebra (Math 1111)<br />'+
            'Recital Attendance (Mus 1000)<br />'+
            'Piano Class (Mus 1501)<br />'+
            'Percussion 1 (Mus 1960)<br />'+
            'University Band (Mus 1010) <br />'+
            'Theory 1 (Mus 1111)<br />'+
            'Aural Training (Mus 1121)<br />'+
            'Intro to Sound Reinforcement (Mus 1410)<br />'+
            'Intro to Audio Production (Mus 1430)<br />'+
            'Piano Class II (Mus 1502)<br />'+
            'Intro to Philosophy (Phil 1000)<br />'+
            'Ethics (Phil 2300)<br />'+
            'Intro to Sciences: Chemistry (Chem 1104)<br />'+
            'British Literature 1750-Present (Engl 2215)<br />'+
            'Experiencing Music (Mus 1210)<br />'+
            'General Psychology (Psy 1100)<br />'+
            'Oral Interpretation(POLS 124) <br />'+
            'Intro to Dietetics (D&n 1300)<br />'+
            'Nutrition (D&n 3340)<br />'+
            'Food Preparation (Food 2322)<br />'+
            'Hotel-Restaurant Sanitation & Safety (Hm 2830)<br />'+
            'Information Resources (Lis 1600)<br />'+
            'General Sociology (Soc 1800)<br />'+
            'Anatomy & Physiology I (Biol 2401)<br />'+
            'Survey of Accounting (Acct 2100)<br />'+
            'Quanitity Food Production & Service (Food 3332)<br />'+
            'Food Systems Management (Food 3333)<br />'+
            'Statistice for the Behavioral Sciences (Psy 4520)<br />'+
            'Anatomy & Physiology II (Biol 2402)<br />'+
            'Community Nutrition (D&n 3350)<br />'+
            'Experimental Foods (D&n 4350)<br />'+
            'Advanced Food Systems Management (Food 3334)<br />'+
            'History US from 1877 (Hist 1351)<br />'+
            'Elementary Organic & Biochemistry (Chem 1604)<br />'+
            'Creative Problem Solving (Igen 3116)<br />'+
            'Stress & Tension Reduction (PE 1204)<br />'+
            'Lifetime Fitness (PE 1206)<br />'+
            'Functional Anatomy (PE 1800)<br />'+
            'Meat Science (Agri 3415)<br />'+
            'Microbiology (Biol 3611)<br />'+
            'Human Anatomy (PE 2800)<br />'+
            'Physiology, Foundations of Physical Education (PE 2850)<br />'+
            'Music of the Morld\'s Cultures (Mus 1225)<br />'+
            'Advanced Nutrition (D&n 4340)<br />'+
            'Medical Nutrition I (D&n 4342)<br />'+
            'Senior Dietetics Seminar (D&n 4345)<br />'+
            'Adapted Physical Education (PE 4340)<br />'+
            'Basic Genetics (Biol 2510)<br />'+
            'Medical Nutrition II (D&n 4343)<br />'+
            'Essentials of Personal Training (PE 2900)<br />'+
            'Assessment & Evaluation of Fitness/Wellness (PE 4850)<br />'+
            'Care & Prevention of Injuries (At 3610)<br />'+
        '</div>'+
        '<div class="modal-footer">'+
            '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">'+
                'Close'+
            '</button>'+
            '<button type="button" class="btn btn-primary">Save changes</button>'+
        '</div>'+
    '</div>'+
'</div>', 
    ];

    document.getElementById('modalText').innerHTML = innerModal[index];
}